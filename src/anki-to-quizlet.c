#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CBUF 2048

struct card {
				char *front;
				char *back;
};

struct htmltag {
				int tagi[2];
				int contenti[2];
				char *tag;
};

struct card cardparse(char *s);
struct htmltag htmlparse(char *s);
void tagdel(char *s, char *tag);
int tagcheck(char *tag);
void tagrep(char *tag, char *s);
void atosup(char *s);
void atosub(char *s);
void constrcards(char **fronts, char **backs, char delim,
		char ncardc, char *dest);

int main(int argc, char *argv[])
{
	FILE *fp;
	char *lineptr;
	size_t len;

	char **s = (char **)malloc(CBUF*sizeof(char *));


	if (argc <= 1) {
		fprintf(stderr, "%s: No input given!\n", argv[0]);
		return -1;
	}

	fp = fopen(argv[1], "r");

	getline(&lineptr, &len, fp);
	for (;strcpy(*s++, lineptr); getline(&lineptr, &len, fp))
		;

	return 0;
}
